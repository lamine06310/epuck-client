# Get all the C++ files composing the epuck library
file(GLOB epuck_FILES ${CMAKE_CURRENT_SOURCE_DIR}/epuck/*.cpp)

# Create a shared library using these files
add_library(epuck SHARED ${epuck_FILES})

# Define the include directory for the library
target_include_directories(epuck PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../include/epuck PRIVATE ${OpenCV_INCLUDE_DIRS})

# Link with OpenCV and pthread libraries
target_link_libraries(epuck ${OpenCV_LIBS} ${CMAKE_THREAD_LIBS_INIT})

# Set the C++ standard to use for this library
target_compile_features(epuck PUBLIC cxx_std_11)

